'use strict';

const express = require('express');

// Constants
const PORT = 8080;
const HOST = '0.0.0.0';

// App
const app = express();
app.get('/', (req, res) => {

    var mysql = require('mysql')
    var database = 'my_db'
    console.log(process.env)
    var connection = mysql.createConnection({
    host     : process.env.MYSQL_HOST || 'localhost',
    user     : process.env.MYSQL_USERNAME || 'root',
    password : process.env.MYSQL_PASSWORD || 'my-secret-pw',
    // database : database
    });

    connection.connect(function(err) {
        if (err) {
            res.send('Backend: Connected. DB: Not Connected.');
            return;
        } 

        connection.query("CREATE DATABASE IF NOT EXISTS " + database, function (err, result) {

            if (err) throw err;
            console.log("Database created");

        })
        
        connection.query('SELECT 1 + 1 AS solution', function (err, rows, fields) {
            if (err) throw err
            res.send('Backend: Connected. DB: Connected.');
        })
        
        connection.end()
    })
    

});

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT} (this port is internal port)`);